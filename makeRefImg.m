rootFolder = 'Z:\Quentin\fUS\Racotin\Iconeus\FilteredData';

imList = dir(fullfile(rootFolder,'*fus2D.source.scan'));
maxT = 100;

makeMask = 0;

for i = 1:length(imList)
    disp(imList(i).name)
    [data, nfo] = loadIconeusData(fullfile(rootFolder,imList(i).name),maxT);
    sName = regexp(imList(i).name,'\d_([a-zA-Z0-9_-]*)_fus2D.source','Tokens');
    sName = sName{1}{1};
    figure;
    imagesc(squeeze(sqrt(mean(data,4)))')
    colormap(gray)
    title(sName, 'Interpreter', 'none')
    if makeMask
        % Draw mask
        % Save mask in param
    end
end