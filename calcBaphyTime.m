function time = calcBaphyTime(nStim,Duration,Repeats)

time = (nStim * Repeats * (Duration + 2)) + (1.5 * Repeats);