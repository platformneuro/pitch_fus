function [data, nfo] = loadIconeusData(filepath, maxT)
% [data, nfo] = loadIconeusData(filepath, maxT)
% Load fUS data from Iconeus (hdf5 format)
% filepath : path to .scan file
% maxT : int, index of last data point to load. Default : all data.
% TO DO: add load for the metadata
% (example : h5read(nfo.Filename,'/acqMetaData/imgDim/dim7')))

nfo = h5info(filepath); % Get HDF5 info, including data set names
dim = nfo.Datasets(1).Dataspace.Size;
start = ones(size(dim));

if exist('maxT','var') && ~isempty(maxT)
    dim(4) = maxT;
end

data = h5read(nfo.Filename,'/Data',start,dim); % Load raw data