function plotB0(b0,stims)

hFig = gcf;
pos = get(hFig,'position');
set(hFig,'DeleteFcn',@closeCallback);

% if ndims(stack) == 3
%     stack = reshape(stack,[size(stack,1) size(stack,2) 1 size(stack,3)]);
% end
image1 = squeeze(b0(:,:,1,1));
displayRange = [min(b0(:)) max(b0(:))];
figure(hFig);
imshow(image1,displayRange,'Border','tight');
title(sprintf('%i/%i - %s - %sHz',1,size(b0,3),stims.stimName{1},stims.f0{1}))
set(hFig,'Position',pos)
hold on

if ishandle(hFig.UserData) % Remove pre-existing slider handles
    posSlider = get(hFig.UserData,'Position');
    delete(hFig.UserData);
else
    posSlider = [0.65 0.6 0.25 0.05];
end

maxSlide = (size(b0,4)*size(b0,3))-1;
sliderhandles = figure('units','normalized','position',posSlider,'menubar','none','toolbar','none','name',sprintf('Navigate slices'));
hSlider = uicontrol(sliderhandles,'Style','slider','units','normalized',...
    'position',[0 0 1 1],'Min',0,'Max',maxSlide,'SliderStep',...
    [1/maxSlide 10/maxSlide],'value',0,'callback',@changeSlice);

handles.image = b0;
handles.h = hFig;
hSlider.UserData = handles;
hSlider.UserData.sweepDur = size(b0,3);
hSlider.UserData.stims = stims;
hFig.UserData = sliderhandles;
end

function changeSlice(hObject, eventdata, handles)
% disp(num2str(get(hObject,'Value')));
sliceIdx = floor(get(hObject,'Value')+1);
r = sliceIdx/hObject.UserData.sweepDur;
t = ((r - floor(r)) * hObject.UserData.sweepDur);
if t == 0, t = hObject.UserData.sweepDur; end
if rem(sliceIdx,hObject.UserData.sweepDur) ~= 0
    r = floor(r)+1;
end
image1 = squeeze(hObject.UserData.image(:,:,round(t),r));
pos = get(hObject.UserData.h,'Position');
figure(hObject.UserData.h);
displayRange = [min(image1(:)) max(image1(:))];
if isnan(displayRange(1))
    displayRange = [0 1];
end
imshow(image1,displayRange,'Border','tight');
title(sprintf('%i/%i - %s - %sHz',uint8(t),hObject.UserData.sweepDur,hObject.UserData.stims.stimName{r},hObject.UserData.stims.f0{r}))
set(hObject.UserData.h,'Position',pos)
end

function closeCallback(hObject, eventdata, handles)
for i = 1:length(hObject.UserData)
    if isvalid(hObject.UserData(i))
        close(hObject.UserData(i))
    end
end
end