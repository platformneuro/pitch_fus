%% load data

clear;
close all;

tmp_S=[];

apply_mask=0; % if you want to select cortex only\

pth = 14;
ferret = 'danc';
stimuli = 'Speech';

ICApath = 'D:\fUS\ICA\';

file = sprintf('AllData_%s_%s%03i.mat',stimuli,ferret,pth);
load(fullfile('data',file),'epoch_bef');
[X,Y,F,R,T,P]=size(epoch_bef);
conditions = {'Before'};

[Ct, labels] = getLabelsFromGTruth(file);

nb_trials = R;
trial_length = T; %length(Data.Trial)/nb_trials;
nb_freq = F;
nb_plane = P;

fs = T/20;
stimWin = floor(11*fs):ceil(14*fs);
RefWin = 1:floor(7*fs); 
timeVect = linspace(0,20,T);

%  Reformat the Matrix of responses epochs into S
tmp_S = epoch_bef;

%  Refernece response in EACH PLANE  for Normalization and Anatomy

%tmp_S  X,Y,F,R,T,P

% bound = 7;

Reference_Resp = squeeze(mean(mean(mean(tmp_S(:,:,:,:,RefWin,:),3),4),5));  % X,Y,F=1,R=1,T=1,P


figure
sliceViewer(Reference_Resp./max(Reference_Resp(:)));

%  Baseline and Normalize all responses by Ref (= Baseline-Response) 

Ref = (nanmean(tmp_S(:,:,:,:,RefWin,:),5)); % X,Y,F,R,T,P
Norm_Resp = (tmp_S - Ref)./Ref; % X,Y,F,R,T,P - renormalize by baseline for each trial

% if apply_mask
%     
%     if ~exist(fullfile('data',['mask_' file]),'file')
%         figure;
%         for p = 1:P
%             im = squeeze(mean(tmp_S(:,:,1,1,:,p),5));
%             imagesc(im);
%             colormap 'hot(256)'
%             ACx(:,:,p) = roipoly;
%         end
%         
%         save(fullfile('data',['mask_' file]),'ACx')
%     else
%         load(fullfile('data',['mask_' file]))
%     end
%     Norm_Resp=Norm_Resp.*reshape(ACx,[size(ACx,1) size(ACx,2) 1 1 1 size(ACx,3)]);
%     Norm_Resp(Norm_Resp==0) = nan;
%     
% end



%% load denoised data (CCA) for same animal

% load(fullfile(ICApath,sprintf('Speech_%s%03i',ferret,pth),'D_norm.mat'))
load('D:\fUS\processedData\Laguiole\Laguiole_01b.mat')
D_norm_reshaped = [];
% Reshape similarly to Norm_Resp
ACx = Ct ~= 'out';
tmp = [];
for s = 1:length(unique(slice_refs{1}))
    msk = squeeze(ACx(:,:,s));
    msk = reshape(msk, [prod(size(msk)) 1]);
    mskID = find(msk);
    
%         idx = find(slice_refs{1}==s) + sum(cellfun(@length,slice_refs(1:s-1)));
    idx = find(slice_refs{1}==s);
    
    tmp = D_norm{1}(:,:,:,idx);
    tmp = permute(tmp,[4 2 3 1]);
    
    tmpMap = nan(length(msk),size(tmp,2),size(tmp,3),size(tmp,4));
    tmpMap(mskID,:,:,:) = tmp;
    tmpMap = reshape(tmpMap,[size(ACx,1) size(ACx,2) size(tmpMap,2) size(tmpMap,3) size(tmpMap,4)]);
    
    D_norm_reshaped(:,:,:,:,:,s) = tmpMap;
end

%% Load ref data
load('C:\Users\Quentin Gaucher\Downloads\b0_Scan_Motor_3D_ML.mat')
figure;
sliceViewer(b0);
%% plot anatomy fct stim (sanity check)
% p = 5;
% 
% for f = 1:F
%     img = squeeze(tmp_S(:,:,f,:,:,p));
%     img = reshape(img,[size(img,1) size(img,2) size(img,3)*size(img,4)]);
%     figure;
%     imagesc(squeeze(mean(img,3)))
% end

%% Visualize activity over time

f = 2;
p = 5;

im = squeeze(tmp_S(:,:,f,:,:,p));
% im = reshape(im,[size(im,1) size(im,2) size(im,3)*size(im,4)]);

im2 = squeeze(Norm_Resp(:,:,f,:,:,p));
% im2 = reshape(im2,[size(im2,1) size(im2,2) size(im2,3)*size(im2,4)]);

clim = [min(im(:)) 60];
clim2 = [-0.5 0.5];

h1 = figure;
h2 = figure;
for r = 1:R
    for t = 1:T
        figure(h1)
        imagesc(squeeze(im(:,:,r,t)))
        title(sprintf('R:%d - F%d/%d',r,t,T))
        set(gca,'clim',clim)
        figure(h2)
        imagesc(squeeze(im2(:,:,r,t)))
        title(sprintf('R:%d - F%d/%d',r,t,T))
        set(gca,'clim',clim2)
        pause(0.2)
    end
end

%% Plot the 5 conditions time-function separately in each plane 
% X,Y,F,R,T,P
names = {'noise' 'speech 25' 'speech 50' 'speech 75' 'speech'};
colors = parula(F);
% data = D_norm_reshaped;
data = Norm_Resp;
for p = 3:P
%     close all
    h1 = figure;
    hold on;
    title(['Plane ', num2str(p)]);
    for f = 1:F
        spatial_avg = squeeze(nanmean(nanmean(data(:,:,f,:,:,p),1),2));
        figure;
        title(['Plane ', num2str(p) ' - ' names{f}])
        hold on
        plot(timeVect,spatial_avg')
        error_area(timeVect,mean(spatial_avg,1),std(spatial_avg,1),'k','k',2);
        hold off;
        
        figure(h1)
        tmp = mean(spatial_avg,1);
%         plot(conv(tmp,triang(3)),'LineWidth',2,'DisplayName',names{f});
        plot(timeVect,tmp,'LineWidth',2,'DisplayName',names{f},'color',colors(f,:));

    end
    ylim = get(gca,'ylim');
    pgon = polyshape([8 8 10 10],[ylim(1) ylim(2) ylim(2) ylim(1)]);
    plot(pgon,'facecolor',[0.8 0.8 0.8],'DisplayName','stimulus')
    set(gca,'ylim',ylim)
    legend('show')
    pause()
end


%% Pixel heatmap
% Plot mean evoked activity on map
Norm_Resp_avg = squeeze(mean(Norm_Resp(:,:,:,:,stimWin,:),5));
Ref_avg = squeeze(mean(Norm_Resp(:,:,:,:,RefWin,:),5));
names = {'noise' 'speech 25' 'speech 50' 'speech 75' 'speech'};
% alphaVal = 0.05/(size(tmp_S,1)*size(tmp_S,2));
alphaVal = 0.01;

Reference_Resp = squeeze(mean(mean(mean(tmp_S(:,:,:,:,RefWin,:),3),4),5));  % X,Y,F=1,R=1,T=1,P

hmap = figure;
hanat = figure;
hoverlay = figure;
hpVal = figure;
for sl = 1:P
    figure(hanat)
    imagesc(log(squeeze(Reference_Resp(:,:,sl))));
    title(sprintf('Avg of plane %i',sl))
    colormap gray
    colorbar
    for st = 1:F
        % ttest
        [h, p] = ttest(squeeze(Ref_avg(:,:,st,:,sl)),squeeze(Norm_Resp_avg(:,:,st,:,sl)),'Dim',3,'Alpha',alphaVal);
        figure(hpVal);
        pMap = p;
        pMap(p>=0.05) = 0.05;
        imagesc(h)
        colormap(flipud(hot))
        colorbar
        
        % Response amplitude
        figure(hmap)
        img = abs(squeeze(mean(Norm_Resp_avg(:,:,st,:,sl),4)));
        imagesc(img)
        title(sprintf('plane %i - stim %s',sl,names{st}))
        colorbar
        
        % overlay
        figure(hoverlay);
        imfuseQ(log(squeeze(Reference_Resp(:,:,sl))),img)
        pause()
    end
end

%% Montage of selectivity contours
% Plot mean evoked activity on map
data = D_norm_reshaped;
% data = Norm_Resp;
Norm_Resp_avg = squeeze(mean(data(:,:,:,:,stimWin,:),5));
Ref_avg = squeeze(mean(data(:,:,:,:,RefWin,:),5));
names = {'noise' 'speech 25' 'speech 50' 'speech 75' 'speech'};
% alphaVal = 0.05/(size(tmp_S,1)*size(tmp_S,2));
alphaVal = 0.05;
colors = parula(size(names,2));

Reference_Resp = squeeze(mean(mean(mean(tmp_S(:,:,:,:,RefWin,:),3),4),5));  % X,Y,F=1,R=1,T=1,P

anatomy = [];
respImg = {};
testImg = {};
for sl = 1:P
    anatomy(:,:,sl) = log(squeeze(Reference_Resp(:,:,sl)));
    
    for st = 1:F
%         % ttest
        [h, p] = ttest(squeeze(Ref_avg(:,:,st,:,sl)),squeeze(Norm_Resp_avg(:,:,st,:,sl)),'Dim',3,'Alpha',alphaVal);
        h(isnan(h)) = 0;
        respImg{st}(:,:,sl) = h;
        testImg{st}(:,:,:,sl) = imfuse(squeeze(anatomy(:,:,sl)),h);
    end
end

for i = 1:F    
    figure;
    h(i) = montage(testImg{i});
    montedImg(:,:,:,i) = get(h(i),'CData');
    title(names{i})
    close
end

figure
sliceViewer(montedImg);

for i = 1:P
        % Get pixel selective for speech regardless of noise
    SelectivityImg(:,:,i) = ~respImg{1}(:,:,i)  & (respImg{2}(:,:,i) &...
        respImg{3}(:,:,i) | respImg{4}(:,:,i) | respImg{5}(:,:,i));
    SelectivityImgFused(:,:,:,i) = imfuse(anatomy(:,:,i),SelectivityImg(:,:,i));
    
 
end
figure;
montage(SelectivityImgFused)

c = 1;
SelResp = [];
for x = 1:size(SelectivityImg,1)
    for y = 1:size(SelectivityImg,2)
        for p = 1:size(SelectivityImg,3)
            if SelectivityImg(x,y,p)
                SelResp(c,:,:,:) = data(x,y,:,:,:,p) ; % X,Y,F,R,T,P - renormalize by baseline for each trial
                c = c + 1;
            end
        end
    end
end
m = squeeze(mean(SelResp,3));
err = squeeze(std(m,1));
m = squeeze(mean(m,1));

figure;
hold on
for j = 1:size(SelResp,2)
    plot(timeVect,m(j,:),'linewidth',2,'color',colors(j,:),'DisplayName',names{j})
end
title('Average response across pixels')
ylim = get(gca,'ylim');
pgon = polyshape([8 8 10 10],[ylim(1) ylim(2) ylim(2) ylim(1)]);
plot(pgon,'facecolor',[0.8 0.8 0.8],'edgecolor',[0.8 0.8 0.8],...
    'edgealpha',0.35,'DisplayName','stimulus')
set(gca,'ylim',ylim)
legend('show')

%% Montage of selectivity contours - TONES
% Plot mean evoked activity on map
% data = D_norm_reshaped;
data = Norm_Resp;
Norm_Resp_avg = squeeze(mean(data(:,:,:,:,stimWin,:),5));
Ref_avg = squeeze(mean(data(:,:,:,:,RefWin,:),5));
names = {'1' '2' '3' '4' '5'};
% alphaVal = 0.05/(size(tmp_S,1)*size(tmp_S,2));
alphaVal = 0.01;
colors = parula(size(names,2));

Reference_Resp = squeeze(mean(mean(mean(tmp_S(:,:,:,:,RefWin,:),3),4),5));  % X,Y,F=1,R=1,T=1,P

anatomy = [];
respImg = false(size(Reference_Resp));
testImg = [];
for sl = 1:P
    anatomy(:,:,sl) = log(squeeze(Reference_Resp(:,:,sl)));
    anatomy = anatomy./ max(anatomy(:));
    
    for st = 1:F
%         % ttest
        [h, p] = ttest(squeeze(Ref_avg(:,:,st,:,sl)),squeeze(Norm_Resp_avg(:,:,st,:,sl)),'Dim',3,'Alpha',alphaVal);
        h(isnan(h)) = 0;
        respImg(:,:,sl) = respImg(:,:,sl) | h;
%         testImg{st}(:,:,:,sl) = imfuse(squeeze(anatomy(:,:,sl)),h);
    end
    testImg(:,:,:,sl) = imfuse(squeeze(anatomy(:,:,sl)),respImg(:,:,sl));
end

for i = 1:F    
    figure;
    h(i) = montage(testImg{i});
    montedImg(:,:,:,i) = get(h(i),'CData');
    title(names{i})
    close
end

figure
sliceViewer(montedImg);

% for i = 1:P
%         % Get pixel selective for speech regardless of noise
%     SelectivityImg(:,:,i) = ~respImg{1}(:,:,i)  & respImg{4}(:,:,i) & ...
%         respImg{5}(:,:,i);
%     SelectivityImgFused(:,:,:,i) = imfuse(anatomy(:,:,i),SelectivityImg(:,:,i));
%     
%  
% end
SelectivityImg = respImg;
figure;
montage(SelectivityImgFused)

c = 1;
SelResp = [];
BFs = zeros(101,128,12);
for x = 1:size(SelectivityImg,1)
    for y = 1:size(SelectivityImg,2)
        for p = 1:size(SelectivityImg,3)
            if SelectivityImg(x,y,p)
                SelResp(c,:,:,:) = data(x,y,:,:,:,p) ; % X,Y,F,R,T,P - renormalize by baseline for each trial
                
                temp = mean(SelResp(c,:,:,:),[3 4]);
                [~,BFtemp] = max(temp);
                BFs(x,y,p) = BFtemp;
                c = c + 1;
            end
        end
    end
end

m = squeeze(mean(SelResp,3));
err = squeeze(ste(m,1));
m = squeeze(mean(m,1));

figure;
hold on
for j = 1:size(SelResp,2)
    plot(timeVect,m(j,:),'linewidth',2,'color',colors(j,:),'DisplayName',names{j})
end
title('Average response across pixels')
ylim = get(gca,'ylim');
pgon = polyshape([8 8 10 10],[ylim(1) ylim(2) ylim(2) ylim(1)]);
plot(pgon,'facecolor',[0.8 0.8 0.8],'edgecolor',[0.8 0.8 0.8],...
    'edgealpha',0.35,'DisplayName','stimulus')
set(gca,'ylim',ylim)
legend('show')


%% Plot ICA results - weight distrib 
clear
rootfolder = ('ICA_jav_pooled');
load(fullfile(rootfolder,'/ICA_results8.mat'))

minVal = min(W_ica_train(:));
maxVal = max(W_ica_train(:));
bins = -0.0625:0.005:0.0625;
binsC = -0.06:0.005:0.06;
for i = 1:size(W_ica_train)
    c(i,:) = histcounts(W_ica_train(i,:),bins);
end

figure;
plot(binsC,c')
xlabel('weigths')
ylabel('pixel number')
title('distribution of pixel weigths for each components')

%% Plot ICA results - evoked response per component

clear
rootfolder = ('ICA_jav_pooled');
load(fullfile(rootfolder,'/ICA_results8.mat'))

names = {'noise' 'speech 25' 'speech 50' 'speech 75' 'speech'};
colors = parula(size(R_ica_train_tc,2));
timeVect = linspace(0,20,size(R_ica_train_tc,1));

for i = 1:size(R_ica_train_tc,3)
    figure;
    hold on
    for j = 1:size(R_ica_train_tc,2)
        plot(timeVect,squeeze(R_ica_train_tc(:,j,i)),'linewidth',2,'color',colors(j,:),'DisplayName',names{j})
    end
    title(sprintf('component %i',i))
    ylim = get(gca,'ylim');
    pgon = polyshape([8 8 10 10],[ylim(1) ylim(2) ylim(2) ylim(1)]);
    plot(pgon,'facecolor',[0.8 0.8 0.8],'edgecolor',[0.8 0.8 0.8],...
        'edgealpha',0.35,'DisplayName','stimulus')
    set(gca,'ylim',ylim)
    legend('show')
end

%% Plot ICA results - Weigth map per component

clear
animal = 'jav';
rootfolder = (['ICA_' animal '_27_denoise_manualOut']);
load(fullfile(rootfolder,'ICA_results10.mat'))
load(fullfile(rootfolder,'D_norm.mat'))

maskList = dir(fullfile('data',['mask_AllData*' animal '*.mat'])); 
anatList = dir(fullfile('data',['anatomy_*' animal '*.mat'])); 

for i = 1:length(maskList)
    allMasks{i} = load(fullfile(maskList(i).folder,maskList(i).name));
    allAnat{i} = load(fullfile(anatList(i).folder,anatList(i).name));
end
allMasks{1} = allMasks{end};
allAnat{1} = allAnat{end};
    
names = {'noise' 'speech 25' 'speech 50' 'speech 75' 'speech'};
colors = parula(size(R_ica_train_tc,2));
timeVect = linspace(0,20,size(R_ica_train_tc,1));
weightMaps = {};

for d = 1:length(slice_refs)
for s = 1:length(unique(slice_refs{d}))
    msk = squeeze(allMasks{d}.ACx(:,:,s));
    msk = reshape(msk, [prod(size(msk)) 1]);
    mskID = find(msk);
    
    idx = find(slice_refs{d}==s) + sum(cellfun(@length,slice_refs(1:d-1)));
    W = W_ica_train(:,idx);
    
    for c = 1:size(W,1)
        
        Wmap = nan(size(msk));
        Wmap(mskID) = W(c,:);
        Wmap = reshape(Wmap,[size(allMasks{d}.ACx,1) size(allMasks{d}.ACx,2)]);
        
        weightMaps{c,d}(:,:,s) = Wmap;
        weightAnat{c,d}(:,:,:,s) = imfuse(allAnat{d}.anatomy(:,:,s),Wmap);
        
%         figure
%         imagesc(Wmap)
%         title(sprintf('weight map rec %i slice %i comp %i',d,s,c))
        
    end
   
end
end

% plots
hCompResp = figure;
hW = figure;
% hW2 = figure;
for c = 1:size(W,1)
    figure(hCompResp);
    clf;
    hold on
    for j = 1:size(R_ica_train_tc,2)
        plot(timeVect,squeeze(R_ica_train_tc(:,j,c)),'linewidth',2,'color',colors(j,:),'DisplayName',names{j})
    end
    title(sprintf('component %i',c))
    ylim = get(gca,'ylim');
    pgon = polyshape([8 8 10 10],[ylim(1) ylim(2) ylim(2) ylim(1)]);
    plot(pgon,'facecolor',[0.8 0.8 0.8],'edgecolor',[0.8 0.8 0.8],...
        'edgealpha',0.35,'DisplayName','stimulus')
    set(gca,'ylim',ylim)
    legend('show')
    hold off
    
    for d = 1:length(slice_refs)
        figure(hW)
        img = weightMaps{c,d};
        img(isnan(img)) = 0;
        DisplayRange = [-max(abs(img(:))) max(abs(img(:)))];
%         DisplayRange = [min(img(:)) max(img(:))];
        montage(weightMaps{c,d},'DisplayRange',DisplayRange)
        colormap(jet)
        colorbar
        title(sprintf('%s - %i',animal,d))
        
%         figure(hW2)
%         montage(weightAnat{c,d})
%         title(sprintf('%s - %i',animal,d))
        
        pause()
    end
end

