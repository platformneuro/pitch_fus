clear
close all

rootDir = fullfile('Y:\','pitchfus');

param.animal = 'Banon';
param.hemis = 2;
param.dataPath = fullfile(rootDir,param.animal,'Iconeus','FilteredData');
param.fileList = dir(fullfile(param.dataPath,'*_fus2D.source.scan'));
param.fileList = {param.fileList.name}';

param.scanList = dir(fullfile(param.dataPath,'*_angio3D.source.scan'));
param.scanList = {param.scanList.name}';

% save anatomy images for each slice
for i = 1:length(param.fileList)
    fprintf('Saving anatomy for slice %i / %i\n',i,length(param.fileList))
    sliceName = strrep(param.fileList{i},'sub-','');
    sliceName = strrep(sliceName,'_ses-Session','');
    sliceName = strrep(sliceName,'_fus2D.source.scan','');
    sliceName = strrep(sliceName,'-','_');
    [data, nfo] = loadIconeusData(fullfile(param.dataPath,param.fileList{i}),100);
    dat = squeeze(data);
    dat = permute(dat,[2 1 3]);
    dat = mean(dat,3);
    param.anatomyImages.(sliceName) = dat;
end

%% Pick a reference scan
scan = makeScan(param.dataPath,param.scanList);
param.scans = scan;
scanNames = fieldnames(param.scans);
for i = 1:length(scanNames)
    figure
    sliceViewer(param.scans.(scanNames{i}))
    title(scanNames{i})
end
r = input(sprintf('pick a ref scan (1-%i) :',length(scanNames)));
param.referenceScanner = scanNames{r};
param.referenceScannerIdx = r;
fprintf('New reference scan : %s\n', scanNames{r})
close all

%% Align slices on ref
figure
sliceViewer(sqrt(param.scans.(scanNames{param.referenceScannerIdx})))
sliceList = fieldnames(param.anatomyImages);
h = figure;
for i = 1:length(param.fileList)
    figure(h);
    imagesc(sqrt(param.anatomyImages.(sliceList{i})))
    colormap(gray)
    title(sliceList{i},'interpreter','none')
    r = input(sprintf('pick corresponding ref image (1-%i) :',size(param.scans.(scanNames{param.referenceScannerIdx}),3)));
    param.fileIdxOnScan(i) = r;
end

%% save param file
fileName = sprintf('param_%s_%.2i.mat',param.animal,param.hemis);
savePath = fullfile(rootDir,param.animal,'processedData');
save(fullfile(savePath,fileName),'param');

disp('Done.')