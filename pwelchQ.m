function hFig = pwelchQ(s,fs,xAxis)
% Wrapper for the pwelch function giving a figure with x axis in Hz or kHz

col = lines;

[pxx,f] = pwelch(s,[],[],round(fs));
hFig = gcf;

if ~exist('xAxis','var') || isempty(xAxis)
    xAxis = 'Hz';
end

switch xAxis
    case 'Hz'
        f = f*round(fs)/(2*pi);
    case 'kHz'
        f = (f*round(fs)/(2*pi)/1000);
end

plot(f,10*log10(pxx),'color',col(5,:))
% title('Welch Power Density Estimate')
xlabel(['Frequency (' xAxis ')'])
ylabel(['P/F (dB/' xAxis ')'])
grid on