function sliceViewer(stack)

hFig = gcf;
pos = get(hFig,'position');
set(hFig,'DeleteFcn',@closeCallback);

if ndims(stack) == 3
    stack = reshape(stack,[size(stack,1) size(stack,2) 1 size(stack,3)]);
end
image1 = squeeze(stack(:,:,:,1));
displayRange = [min(image1(:)) max(image1(:))];
figure(hFig);
imshow(image1,displayRange,'Border','tight');
title(sprintf('%i/%i',1,size(stack,4)))
set(hFig,'Position',pos)
hold on

if ishandle(hFig.UserData) % Remove pre-existing slider handles
    posSlider = get(hFig.UserData,'Position');
    delete(hFig.UserData);
else
    posSlider = [0.65 0.6 0.25 0.05];
end

sliderhandles = figure('units','normalized','position',posSlider,'menubar','none','toolbar','none','name',sprintf('Navigate slices'));
hSlider = uicontrol(sliderhandles,'Style','slider','units','normalized',...
    'position',[0 0 1 1],'Min',0,'Max',size(stack,4)-1,'SliderStep',...
    [1/(size(stack,4)-1) 10/(size(stack,4)-1)],'value',0,'callback',@changeSlice);

handles.image = stack;
handles.h = hFig;
hSlider.UserData = handles;

hFig.UserData = sliderhandles;
end

function changeSlice(hObject, eventdata, handles)
% disp(num2str(get(hObject,'Value')));
sliceIdx = floor(get(hObject,'Value')+1);
image1 = sqrt(squeeze(hObject.UserData.image(:,:,:,sliceIdx)));
pos = get(hObject.UserData.h,'Position');
figure(hObject.UserData.h);
displayRange = [min(image1(:)) max(image1(:))];
imshow(image1,displayRange,'Border','tight');
title(sprintf('%i/%i',sliceIdx,hObject.Max+1))
set(hObject.UserData.h,'Position',pos)
end

function closeCallback(hObject, eventdata, handles)
for i = 1:length(hObject.UserData)
    if isvalid(hObject.UserData(i))
        close(hObject.UserData(i))
    end
end
end