clear
close all

dataPath = 'Y:\pitchfus\Banon\Iconeus\FilteredData';
fileName = 'sub-Banon_ses-Session_2021-11-24_FTC_02m_fus2D.source.scan';

filePath = fullfile(dataPath,fileName);
fs = 1/0.4;

[data, nfo] = loadIconeusData(filePath);

data = squeeze(data);
%% Plot raw activity in region
% Img = mean(data,[3]);
% figure;
% imagesc(Img)
flim = [1/5 1/7];
masks = drawManualMask(data);
param.ManualMask = masks.cortex;
act = Mat2Pixs(data,param);
avgAct = mean(act);
t = (0:length(avgAct)-1) * 1/fs;
figure;
plot(t,avgAct)

L = length(avgAct);
Ft = fft(avgAct);
P2 = abs(Ft/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = fs*(0:(L/2))/L;
figure
plot(f,P1)
hold on
ylim = get(gca,'ylim');
pgon = polyshape([flim(1) flim(1) flim(2) flim(2)],[ylim(2) ylim(1) ylim(1) ylim(2)]);
plot(pgon)
set(gca,'ylim',ylim)
title('Selection spectrum')
xlabel('frequency (Hz)')
ylabel('power')

% all pixel spectrum
for p = 1:size(act,1)
    Ft = fft(act(p,:));
    P2 = abs(Ft/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    allFT(p,:) = P1;
end
[v,isort] = sort(max(allFT(:,floor(flim(1)\fs):ceil(flim(2)\fs)),[],2));
figure;
imagesc(f,1:size(allFT,1),allFT(isort,:))
hold on
line([flim(1) flim(2); flim(1) flim(2)],[1 1;size(allFT,1) size(allFT,1) ],'color','r')
xlabel('frequency (Hz)')
ylabel('pixel')
set(gca,'clim',[0 0.5])