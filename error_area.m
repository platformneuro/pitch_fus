function [ h ] = error_area( x,y,e,curve_col,area_col,linewidth,scale,X_ticks_label, Y_ticks_label)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

error(nargchk(3,9,nargin));

x = x(:)'; y = y(:)'; e = e(:)';

if ~exist('curve_col','var') || isempty(curve_col)
    curve_col = [0 0 1];
end
if ~exist('area_col','var') || isempty(area_col)
    area_col = curve_col;
end
if ~exist('linewidth','var') || isempty(linewidth)
    linewidth = 1.2;
end
if ~exist('scale','var') || isempty(scale)
    scale = 'linear';
end
if exist('X_ticks_label','var') && length(X_ticks_label)>=2
    uwx = true;
else uwx = false;
end
if exist('Y_ticks_label','var') && length(Y_ticks_label)>=2
    uwy = true;
else uwy = false;
end

nvlx = false; nvly = false;

switch scale
    case 'linear'
        if uwx    
            lim_x = [X_ticks_label(1) X_ticks_label(end)];
            lim_x_log = lim_x;
            X_ticks = X_ticks_label; 
            nvlx = true;
        end
        if uwy   
            lim_y = [Y_ticks_label(1) Y_ticks_label(end)];
            lim_y_log = lim_y;
            Y_ticks = Y_ticks_label;
            nvly = true;
        end
        X = x; Y = y;
        stack1 = Y-e; stack2 = 2*e;
        
    case 'xlog'
        under1 = x < 1;
        x = x(1,~under1);
        y = y(1,~under1);
        e = e(1,~under1);
        
        if ~uwx
            lim_x = [min(x) max(x)];
            lim_x_log = [floor(log10(lim_x(1))) ceil(log10(lim_x(2)))];
            
            X_ticks = linspace(lim_x_log(1), lim_x_log(2), lim_x_log(2)+1);
            X_ticks_label = logspace(lim_x_log(1), lim_x_log(2), lim_x_log(2)+1);
            nvlx = true;
        else
            lim_x = [X_ticks_label(1) X_ticks_label(end)];
            lim_x_log = [log10(lim_x(1)) log10(lim_x(2))];
            
            X_ticks = log10(X_ticks_label);
            nvlx = true;
        end
        if uwy
            lim_y = [Y_ticks_label(1) Y_ticks_label(end)];
            lim_y_log = lim_y;
            Y_ticks = Y_ticks_label;
            nvly = true;
        end
        
        X = log10(x);
        Y = y;
        stack1 = Y-e; stack2 = 2*e;
        
    case 'ylog'
        under1 = y < 1;
        x = x(1,~under1);
        y = y(1,~under1);
        e = e(1,~under1);
        
        if uwx
            lim_x = [X_ticks_label(1) X_ticks_label(end)];
            lim_x_log = lim_x;
            X_ticks = X_ticks_label;
            nvlx = true;
        end 
        if ~uwy
            lim_y = [min(y) max(y)];
            lim_y_log = [floor(log10(lim_y(1))) ceil(log10(lim_y(2)))];
            
            Y_ticks = linspace(lim_y_log(1), lim_y_log(2), lim_y_log(2)+1);
            Y_ticks_label = logspace(lim_y_log(1), lim_y_log(2), lim_y_log(2)+1);
            nvly = true;
        else
            lim_y = [Y_ticks_label(1) Y_ticks_label(end)];
            lim_y_log = [log10(lim_y(1)) log10(lim_y(2))];
            
            Y_ticks = log10(Y_ticks_label);
            nvly = true;
        end
        
        X = x;
        Y = log10(y);
        
        stack1 = log10(y-e);
        stack2 = log10((y+e)./(y-e));
        %stack2 = log10((2*e)./(y-e));
        
    case 'loglog'
        under1 = x < 1 | y < 1;
        x = x(1,~under1);
        y = y(1,~under1);
        e = e(1,~under1);
        
        if ~uwx
            lim_x = [min(x) max(x)];
            lim_x_log = [floor(log10(lim_x(1))) ceil(log10(lim_x(2)))];
            
            X_ticks = linspace(lim_x_log(1), lim_x_log(2), lim_x_log(2)+1);
            X_ticks_label = logspace(lim_x_log(1), lim_x_log(2), lim_x_log(2)+1);
            nvlx = true;
        else
            lim_x = [X_ticks_label(1) X_ticks_label(end)];
            lim_x_log = [log10(lim_x(1)) log10(lim_x(2))];
            
            X_ticks = log10(X_ticks_label);
            nvlx = true;
        end
        if ~uwy
            lim_y = [min(y) max(y)];
            lim_y_log = [floor(log10(lim_y(1))) ceil(log10(lim_y(2)))];
            
            Y_ticks = linspace(lim_y_log(1), lim_y_log(2), lim_y_log(2)+1);
            Y_ticks_label = logspace(lim_y_log(1), lim_y_log(2), lim_y_log(2)+1);
            nvly = true;
        else
            lim_y = [Y_ticks_label(1) Y_ticks_label(end)];
            lim_y_log = [log10(lim_y(1)) log10(lim_y(2))];
            
            Y_ticks = log10(Y_ticks_label);
            nvly = true;
        end
        
        X = log10(x);
        Y = log10(y);
        
        stack1 = log10(y-e);
        %stack1 = log10(y./e);
        
        stack2 = log10((y+e)./(y-e));
        %stack2 = log10((2*e)./(y-e));
%         for i = 1:length(e)
%             stack2(i) = log10(e(i)^2);
%         end
        
    case 'dB' % y axis in dB, x axis in log10(x)
        under1 = x < 1 | y < 1;
        x = x(1,~under1);
        y = y(1,~under1);
        e = e(1,~under1);
        
        if ~uwx
            lim_x = [min(x) max(x)];
            lim_x_log = [floor(log10(lim_x(1))) ceil(log10(lim_x(2)))];
            
            X_ticks = linspace(lim_x_log(1), lim_x_log(2), lim_x_log(2)+1);
            X_ticks_label = logspace(lim_x_log(1), lim_x_log(2), lim_x_log(2)+1);
            nvlx = true;
        else
            lim_x = [X_ticks_label(1) X_ticks_label(end)];
            lim_x_log = [log10(lim_x(1)) log10(lim_x(2))];
            
            X_ticks = log10(X_ticks_label);
            nvlx = true;
        end
        if uwy
            lim_y = [Y_ticks_label(1) Y_ticks_label(end)];
            lim_y_log = lim_y;
            Y_ticks = Y_ticks_label;
            nvly = true;
        end
        
        X = log10(x);
        max_spectrum = max(y);
        Y = 20*log10(y./max_spectrum);
        
        stack1 = 20*log10((y-e)./max_spectrum);
        stack2 = 20*log10((y+e)./(y-e));
        %stack2 = 20*log10((2*e)./(y-e));
        
    otherwise
        error('Unexpected scale type. Possible values are linear, xlog, ylog, loglog, dB');
end
ylim = get(gca,'YLim');
h1 = area(X,[stack1;stack2]','EdgeColor','none','FaceColor',area_col);
% set(get(h1(2),'children'),'facealpha',0.2);
set(h1(2),'facealpha',0.2);
set(h1(1),'facecolor','none')
set(gca,'YLim',ylim)

initial = ishold;

if ~initial
hold on
    h3 = plot(X,Y,'marker','none','LineWidth',linewidth,'color',curve_col);
hold off
else
    h3 = plot(X,Y,'marker','none','LineWidth',linewidth,'color',curve_col);
end

if nvlx
xlim(lim_x_log);
set(gca,'xtick',X_ticks);
set(gca,'xticklabel',X_ticks_label);
end
if nvly
ylim(lim_y_log);
set(gca,'ytick',Y_ticks);
set(gca,'yticklabel',Y_ticks_label);
end

h = [h3 h1];

end

