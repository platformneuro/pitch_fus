%% parameters

rootDir = 'Z:\Quentin\fUS\';
animal = 'Laguiole';
slice = '01e';
isCCA = 0;

if isCCA
    ccaTag = '_corr2';
else
    ccaTag = '';
end

fileName = fullfile(rootDir,animal,'processedData',[animal '_' slice ccaTag '.mat']);
% load('D:\fUS\Pitch\Laguiole\processedData\Laguiole_01e.mat')
load(fileName)

if isCCA
    b0 = Din_c;
end

if ~isfield(param,'anatomy')
    param.anatomy = sqrt(squeeze(mean(b0,[3 4])));
end
    
figure;
plotOverlay(param);

%% Compute average activity per stim

if ~isfield(param,'ManualMask')
    param.ManualMask = param.msk.ManualMask;
end

stimList = unique(stims.stimName);
f0 = unique(stims.f0);

meanAct = zeros(size(b0,1),size(b0,2),size(b0,3),length(stimList)*length(f0));
errAct  = zeros(size(meanAct));
meanR = zeros(size(b0,1),size(b0,2),length(stimList)*length(f0));
errR = zeros(size(meanR));

R = squeeze(max(b0(:,:,1:7,:),[],3) - min(b0(:,:,12:13,:),[],3));
b0Pix = Mat2Pixs(b0,param);

c = 0;
for i = 1:length(stimList)
    for j = 1:length(f0)
        c = c + 1;
        ref{c,1} = stimList{i};
        ref{c,2} = f0{j};
        idx =  find(strcmp(stims.stimName,stimList{i}) & strcmp(stims.f0,f0{j}));
        if max(idx) > size(b0,4) % in case of interrupted recording, remove interrupted trial.
            idx = idx(idx~=max(idx));
        end
        meanAct(:,:,:,c) = squeeze(nanmean(b0(:,:,:,idx),4));
        errAct(:,:,:,c) = squeeze(nanstd(b0(:,:,:,idx),[],4));
        meanR(:,:,c) = squeeze(mean(R(:,:,idx),3));
        errR(:,:,c) = squeeze(std(R(:,:,idx),[],3));
    end
end

%% ANOVA2 on stim and f0
Rpix = Mat2Pixs(R,param);

% Remove interrupted trial if recording finished before Baphy
stimList = stims.stimName(1:size(b0,4));
f0List = stims.f0(1:size(b0,4));

f0ToAnalyze = {'1189','2000','250','3364','420','707'};

% stimToAnalyze = {'IRN0','IRN1','IRN3','IRN9'};
stimToAnalyze = {'CT0','CT15','CT40','IRN0','IRN1','IRN3','IRN9'};


istim = false(size(stimList));
for i = 1:length(stimToAnalyze)
    istim = istim | strcmp(stimList,stimToAnalyze{i});
end

groups = {stimList(istim),f0List(istim)};
p = zeros(size(Rpix,1),3);
for i = 1:size(Rpix,1)
    y = squeeze(Rpix(i,istim));
    [p(i,[1 2 3]),tbl,stats,terms] = anovan(y,groups,'display','off','model','interaction');
    
    % "true pitch test"
    [h1,p(i,4),ci1,stats1] = ttest(squeeze(Rpix(i,strcmp(stimList,'CT0'))),squeeze(Rpix(i,strcmp(stimList,'CT40'))),'tail','right');
    [h2,p(i,5),ci2,stats2] = ttest(squeeze(Rpix(i,strcmp(stimList,'IRN9'))),squeeze(Rpix(i,strcmp(stimList,'IRN0'))),'tail','left');

end

for i = 1:size(p,2)
    fprintf('%i pixels significant\n',sum(p(:,i)<0.05));
    v = p(:,i);
    v(v>0.05) = 0.05;
    
    figure;
    imagesc(Pixs2Mat(v,param))
    colormap(1-gray)
    
end

v = p(:,1)<0.05 & p(:,4)<0.05 & p(:,5)<0.05;
figure;
imagesc(Pixs2Mat(v,param))
colormap(1-gray)
title('Putative pitch voxels')
%% plot average traces
% for i = 1:3
clear avg
idxPix = find(p(:,1)<0.05);
t = linspace(0,13*0.4,13);
c = 1;
for i = 1:length(stimToAnalyze)
    h(c) = figure;
    c = c + 1;
end
c = 1;
for i = idxPix'
    for j = 1:length(stimToAnalyze)
        for k = 1:length(f0ToAnalyze)
            idxStim = strcmp(stimList,stimToAnalyze{j}) & strcmp(f0List,f0ToAnalyze{k});
            avg(k,:) = nanmean(squeeze(b0Pix(i,:,idxStim)),2);
            err(k,:) = nanstd(squeeze(b0Pix(i,:,idxStim)),[],2); 
        end
        figure(c)
        c = c + 1;
        plot(t,avg')
        colororder(jet(length(f0ToAnalyze)))
        hold on
        error_area(t,mean(avg),mean(err));
        hold off
        title(stimToAnalyze{j})
        legend(f0ToAnalyze)
        xlabel('time (sec)')
        ylabel('fUS')
        clear avg err
    end
    pause;
%     close all
    c = 1;
end

