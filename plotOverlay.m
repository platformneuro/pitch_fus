function hfig = plotOverlay(param,img,alpha)

if ~exist('alpha','var') || isempty(alpha)
    alpha = 1;
end

anat = param.anatomy ./ max(param.anatomy(:));

hfig = gcf;
imagesc(anat)
colormap(gray)
hold on
contour(param.ManualMask,'color','red','levels',1)
if exist('img','var') && ~isempty(img)
    img = img ./ max(img(:));
    overlay = cat(3, ones(size(img)), zeros(size(img)), zeros(size(img)));
    h = imshow(overlay);
    set(h, 'AlphaData', img .* alpha);
end