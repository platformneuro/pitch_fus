clear
close all

animal = 'Banon';
hemis = '02';

dataRoot = fullfile('Y:\', 'pitchfus');
dataPath = fullfile(dataRoot,animal,'processedData');

load(fullfile(dataPath,['param_' animal '_' hemis '.mat']))

stims = cellfun(@(x)(regexp(x,'-\d+_(\D{1,3})_','tokens')),param.fileList);
stims = cellfun(@(x)(x{1}),stims,'UniformOutput',false);

stimList = unique(stims);

I = param.scans.(param.referenceScanner);
I = sqrt(I./max(I(:)));

for i = 1:length(stimList)
    I2 = I;
    frameIdx = param.fileIdxOnScan(strcmp(stims,stimList{i}));
    I2([1:3],:,frameIdx) = 1;
    figure;
    montage(I2)
    title(sprintf('%s %s %s',animal,hemis,stimList{i}))
    saveas(gcf,fullfile(dataPath,sprintf('%s_%s_%s_%s_scanAlignment.tif',animal,hemis,stimList{i})),'tiffn')
end